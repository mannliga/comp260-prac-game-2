﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision) {
		Debug.Log ("Collision Enter" + collision.gameObject.name);
		if (paddleLayer.Contains (collision.gameObject)) {
			//hit the paddle
			audio.PlayOneShot (paddleCollideClip);
		} else {
			//hit something else
			audio.PlayOneShot (wallCollideClip);
		}
	}

	void OnCollisionStay(Collision collision) {
		Debug.Log ("Collision Stay" + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log ("Collision Exit" + collision.gameObject.name);
	}

}
