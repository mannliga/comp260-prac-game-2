﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour 
{
	public float speed = 20f;
	private Rigidbody rbody;

	void Start () 
	{
		rbody = GetComponent<Rigidbody> ();
		rbody.useGravity = false;
	}

	private Vector3 GetMousePosition() {
		//create a ray from the camera
		//passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		//find out where the ray intersects the XY plane
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}

	// Use this for initialization	
	void Update() {
	
	}

	// Update is called once per frame
	public float force = 10f;

	void FixedUpdate () 
	{
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rbody.position;
		Vector3 vel = dir.normalized * speed;
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;
		rbody.AddForce (dir.normalized * force);

		if (move > distToTarget) {
			vel = vel * distToTarget / move;
		}
		rbody.velocity = vel;
	}
	
	void OnDrawGizmos() 
	{
		//draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
}
